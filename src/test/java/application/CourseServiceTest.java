package application;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import api.utils.CreateFullCourseRequest;
import api.utils.CreatePartantRequest;
import api.utils.Erreur;
import api.utils.ErreursRef;
import dao.repo.CourseCompositionRepository;
import dao.repo.CourseRepository;
import dao.repo.PartantRepository;
import kafka.service.KafkaProducer;
import model.Course;
import model.CourseComposition;
import model.Partant;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class CourseServiceTest {
	
	private final KafkaProducer producer = new KafkaProducer();
	private static Course course;
	private static Partant partant1;
	private static Partant partant2;
	private static Partant partant3;
	private static CourseComposition courseCompo1;
	private static CourseComposition courseCompo2;
	private static CourseComposition courseCompo3;
	private static Date date  = getDate();
	
	
	@Autowired
    public CourseRepository courseRepository;
	
	@Autowired
    public PartantRepository partantRepository;
	
	@Autowired 
	public CourseCompositionRepository courseCompoRepository;
	
	@Autowired
	private CoursesService coursesService;
	
	@BeforeEach //TODO vérifier pourquoi cette annotation n'est pas prise en compte
	public void setUp() {
		
		Date date = getDate();
		
		partant1.setNom("TEST_PARTANT_1");
		partant1.setNumero(123);
		partantRepository.save(partant1);
		
		partant2.setNom("TEST_PARTANT_2");
		partant2.setNumero(1234);
		partantRepository.save(partant2);
		
		partant3.setNom("TEST_PARTANT_1");
		partant3.setNumero(12345);
		partantRepository.save(partant3);
		
		course.setNom("COURSE_TEST_1");
		course.setNumero(123456);
		course.setDateCourse(date);
		courseRepository.save(course);
		
		courseCompo1.setCourse(course);
		courseCompo1.setPartant(partant1);
		courseCompo1.setNumeroPartant(1);
		courseCompoRepository.save(courseCompo1);
		
		courseCompo2.setCourse(course);
		courseCompo2.setPartant(partant2);
		courseCompo2.setNumeroPartant(2);
		courseCompoRepository.save(courseCompo2);
		
		courseCompo3.setCourse(course);
		courseCompo3.setPartant(partant3);
		courseCompo3.setNumeroPartant(3);
		courseCompoRepository.save(courseCompo3);
		
		
	}
	
	@AfterEach//TODO vérifier pourquoi cette annotation n'est pas prise en compte
	public void tearDown() {
		
		courseCompoRepository.deleteAll();
		partantRepository.deleteAll();
		courseRepository.deleteAll();
	}

	
	
	@Test
	public void contextLoads() throws Exception {
		assertThat(coursesService).isNotNull();
	}
	
	
	@Test
	public void injectedComponentsAreNotNull(){
		assertThat(courseRepository).isNotNull();
		assertThat(partantRepository).isNotNull();
		assertThat(courseCompoRepository).isNotNull();
	}
	
	@Test
	public void testCreateCourse() throws NoSuchFieldException, SecurityException {
		
		createDataTest();
	
		CreateFullCourseRequest req = new CreateFullCourseRequest();
		req.setNom("COURSE_TEST_OK");
		req.setNumero(123456L);
		req.setDateCourse(date);
		List<CreatePartantRequest> partantList = new ArrayList<CreatePartantRequest>();
		CreatePartantRequest partant1 = new CreatePartantRequest();
		partant1.setNom("TEST_PARTANT_1");
		partantList.add(partant1);
		CreatePartantRequest partant2 = new CreatePartantRequest();
		partant2.setNom("TEST_PARTANT_2");
		partantList.add(partant2);
		CreatePartantRequest partant3 = new CreatePartantRequest();
		partant3.setNom("TEST_PARTANT_3");
		partantList.add(partant3);
		req.setPartantsList(partantList);	
		
		ResponseEntity<Erreur> responseEntity = coursesService.createFullCourse(req, null);
		Assert.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		
		removeDataTest();
	}
	
	@Test
	public void testCreateExistingCourse() throws NoSuchFieldException, SecurityException {
		
		createDataTest();
	
		CreateFullCourseRequest req = new CreateFullCourseRequest();
		req.setNom("COURSE_TEST_1");
		req.setNumero(123456L);
		req.setDateCourse(date);
		List<CreatePartantRequest> partantList = new ArrayList<CreatePartantRequest>();
		CreatePartantRequest partant1 = new CreatePartantRequest();
		partant1.setNom("TEST_PARTANT_1");
		partantList.add(partant1);
		CreatePartantRequest partant2 = new CreatePartantRequest();
		partant2.setNom("TEST_PARTANT_2");
		partantList.add(partant2);
		CreatePartantRequest partant3 = new CreatePartantRequest();
		partant3.setNom("TEST_PARTANT_3");
		partantList.add(partant3);
		req.setPartantsList(partantList);	
		
		ResponseEntity<Erreur> responseEntity = coursesService.createFullCourse(req, null);
		Assert.assertEquals(HttpStatus.CONFLICT, responseEntity.getStatusCode());
		Assert.assertEquals(ErreursRef.COURSE_ALREADY_EXISTS.getCode(), responseEntity.getBody().getCode());
		
		removeDataTest();
	}
	
	@Test
	public void testCreateCourseWithout3Elements() throws NoSuchFieldException, SecurityException {
		
		createDataTest();
		
		CreateFullCourseRequest req = new CreateFullCourseRequest();
		req.setNom("COURSE_TEST_3");
		req.setNumero(2L);
		req.setDateCourse(date);
		List<CreatePartantRequest> partantList = new ArrayList<CreatePartantRequest>();
		CreatePartantRequest partant1 = new CreatePartantRequest();
		partant1.setNom("TEST_PARTANT_1");
		partantList.add(partant1);
		CreatePartantRequest partant2 = new CreatePartantRequest();
		partant2.setNom("TEST_PARTANT_2");
		partantList.add(partant2);
		req.setPartantsList(partantList);
		
		
		ResponseEntity<Erreur> responseEntity = coursesService.createFullCourse(req, null);
		Assert.assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		Assert.assertEquals(ErreursRef.MINIMUM_PARTANT_NOT_SATISFIED.getCode(), responseEntity.getBody().getCode());
		
		removeDataTest();
		
	}
	
	@Test
	public void testCreateCourseWithPartantNotExists() throws NoSuchFieldException, SecurityException {
	
		CreateFullCourseRequest req = new CreateFullCourseRequest();
		req.setNom("COURSE_TEST_3");
		req.setNumero(2L);
		req.setDateCourse(date);
		List<CreatePartantRequest> partantList = new ArrayList<CreatePartantRequest>();
		CreatePartantRequest partant1 = new CreatePartantRequest();
		partant1.setNom("TEST_PARTANT_1");
		partantList.add(partant1);
		CreatePartantRequest partant2 = new CreatePartantRequest();
		partant2.setNom("TEST_PARTANT_2");
		partantList.add(partant2);
		CreatePartantRequest partant3 = new CreatePartantRequest();
		partant3.setNom("TEST_PARTANT_3");
		partantList.add(partant3);
		req.setPartantsList(partantList);	
		
		ResponseEntity<Erreur> responseEntity = coursesService.createFullCourse(req, null);
		Assert.assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		Assert.assertEquals(ErreursRef.PARTANTS_NOT_FOUND.getCode(), responseEntity.getBody().getCode());
		
		removeDataTest();
	}
	
	public static Date getDate() {
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		date = cal.getTime();
		
		return date;
	}
	
	public void createDataTest() {
		partant1 = new Partant();
		partant1.setNom("TEST_PARTANT_1");
		partant1.setNumero(123);
		partantRepository.save(partant1);
		
		partant2 = new Partant();
		partant2.setNom("TEST_PARTANT_2");
		partant2.setNumero(1234);
		partantRepository.save(partant2);
		
		partant3 = new Partant();
		partant3.setNom("TEST_PARTANT_3");
		partant3.setNumero(12345);
		partantRepository.save(partant3);
		
		course = new Course();
		course.setNom("COURSE_TEST_1");
		course.setNumero(123456);
		course.setDateCourse(date);
		courseRepository.save(course);
		
		courseCompo1 = new CourseComposition();
		courseCompo1.setCourse(course);
		courseCompo1.setPartant(partant1);
		courseCompo1.setNumeroPartant(1);
		courseCompoRepository.save(courseCompo1);
		
		courseCompo2 = new CourseComposition();
		courseCompo2.setCourse(course);
		courseCompo2.setPartant(partant2);
		courseCompo2.setNumeroPartant(2);
		courseCompoRepository.save(courseCompo2);
		
		courseCompo3 = new CourseComposition();
		courseCompo3.setCourse(course);
		courseCompo3.setPartant(partant3);
		courseCompo3.setNumeroPartant(3);
		courseCompoRepository.save(courseCompo3);	
	}
	
	public void removeDataTest() {
		courseCompoRepository.deleteAll();
		partantRepository.deleteAll();
		courseRepository.deleteAll();
	}

}
