package application;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import api.utils.Constantes;
import api.utils.CreateFullCourseRequest;
import api.utils.CreateFullCourseResponse;
import api.utils.DeleteCourseResponse;
import api.utils.Erreur;
import api.utils.GetCoursesResponse;
import api.utils.Utils;
import controller.CoursesController;
import dao.repo.CourseCompositionRepository;
import dao.repo.CourseRepository;
import dao.repo.PartantRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import kafka.service.KafkaProducer;

@RestController
@RequestMapping("/courses")
@Tag(name = "Courses Service", description = "API pour la gestion des courses.")
public class CoursesService {
	
	private static final Logger log = Logger.getLogger(Constantes.WES_LOGGER);
	
	private final CoursesController controller;
	private final KafkaProducer producer;
	
	@Autowired
    CourseRepository courseRepository;
	
	@Autowired
    PartantRepository partantRepository;
	
	@Autowired
	CourseCompositionRepository courseCompositionRepository;
	
	
	@Autowired
    public CoursesService(final CoursesController controller, KafkaProducer producer) {
        this.controller = controller;
        this.producer = producer;
    }
	
	@SuppressWarnings("unchecked")
	@GetMapping("")
	@Operation(summary = "webservice qui permet de lister les courses existantes")
	@ApiResponses(value = {
		      @ApiResponse(responseCode = "200", description = "ok", content = @Content(schema = @Schema(implementation = GetCoursesResponse.class)))})
	public <T> ResponseEntity<T> getCourses() {
		log.info("Début du webservice getCourses");
		
		final GetCoursesResponse response = controller.getCourses();
		if (response.getErreur() != null) {
            return (ResponseEntity<T>) ResponseEntity.status(response.getErreur().getStatus())
                    .body(new Erreur(response.getErreur()));
        }
		
		log.info("Fin du webservice getCourses");
		return (ResponseEntity<T>) ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@SuppressWarnings("unchecked")
	@Operation(summary = "webservice qui permet la création d'une course. Pour qu'un participant puisse être ajouté dans une course il faut qu'il soit créé au préalable dans le référentiel des participants via l'api /participants")
	@ApiResponses(value = {
		      @ApiResponse(responseCode = "201", description = "course créée"),
		      @ApiResponse(responseCode = "400", description = "Body en entrée érroné // La course doit posséder au minimum 3 partants", content = @Content),
		      @ApiResponse(responseCode = "404", description = "Partant non trouvé", content = @Content),
		      @ApiResponse(responseCode = "409", description = "La course existe déja", content = @Content)})
	@PostMapping("")
	public <T> ResponseEntity<T> createFullCourse(@Valid @RequestBody CreateFullCourseRequest courseRequest, Errors errors){
		
		log.info("Début du webservice createFullCourse");
		
		if (errors != null && errors.hasErrors()) {
			String erreur = Utils.getValidationErrors(errors);
			return (ResponseEntity<T>) ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new Erreur(2, erreur));
	    }
		
		final CreateFullCourseResponse response = controller.createFullCourse(courseRequest);
		if (response.getErreur() != null) {
            return (ResponseEntity<T>) ResponseEntity.status(response.getErreur().getStatus())
                    .body(new Erreur(response.getErreur()));
        }
		
		//this.producer.sendMessage(courseRequest); //TODO décomenter pour l'envoie par kafka
		
		log.info("Fin du webservice createFullCourse");
		return ResponseEntity.status(HttpStatus.CREATED).body(null);
	}
	
	
	@SuppressWarnings("unchecked")
	@Operation(summary = "webservice qui permet la suppression d'une course")
	@ApiResponses(value = {
		      @ApiResponse(responseCode = "200", description = "course supprimée"),
		      @ApiResponse(responseCode = "404", description = "Course non trouvée", content = @Content)})
	@DeleteMapping("/{id}")
	public <T> ResponseEntity<T> deleteCourse(@PathVariable Long id){
		
		log.info("Début du webservice deleteCourse");
		
		final DeleteCourseResponse response = controller.deleteCourse(id);
		if (response.getErreur() != null) {
            return (ResponseEntity<T>) ResponseEntity.status(response.getErreur().getStatus())
                    .body(new Erreur(response.getErreur()));
        }
		
		log.info("Fin du webservice deleteCourse");
		return ResponseEntity.status(HttpStatus.OK).body(null);
	}

}
