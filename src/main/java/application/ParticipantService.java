package application;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import api.utils.Constantes;
import api.utils.CreateFullCourseResponse;
import api.utils.CreatePartantRequest;
import api.utils.Erreur;
import api.utils.GetPartantListResponse;
import api.utils.Utils;
import controller.PartantController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/participants")
@Tag(name = "Participants Service", description = "API pour la gestion des participants. Pour qu'un participant puisse être ajouté dans une course il faut qu'il soit créé au préalable dans le référentiel des participants via cette API.")
public class ParticipantService<T> {
	
	private static final Logger log = Logger.getLogger(Constantes.WES_LOGGER);
	
	private final PartantController controller;
	
	@Autowired
    public ParticipantService(final PartantController controller) {
        this.controller = controller;
    }
	
	@SuppressWarnings("unchecked")
	@Operation(summary = "Référentiel des participants présents dans la base de données")
	@ApiResponses(value = {
		      @ApiResponse(responseCode = "200", description = "ok", content = @Content(schema = @Schema(implementation = GetPartantListResponse.class)))})
	@GetMapping("")
	public <T> ResponseEntity<T> getPartantsList() {
		
		log.info("Début du webservice getPartantsList");
		
		final GetPartantListResponse response = controller.getPartantsList();
		if (response.getErreur() != null) {
            return (ResponseEntity<T>) ResponseEntity.status(response.getErreur().getStatus())
                    .body(new Erreur(response.getErreur()));
        }
		
		log.info("Fin du webservice getPartantsList");
		return (ResponseEntity<T>) ResponseEntity.status(HttpStatus.OK).body(response);
		
	}
	
	@SuppressWarnings("unchecked")
	@PostMapping("")
	@Operation(summary = "webservice qui permet la création de nouveaux participants dans le référentiel des participants")
	@ApiResponses(value = {
		      @ApiResponse(responseCode = "201", description = "participant créé"),
		      @ApiResponse(responseCode = "400", description = "Body en entrée érroné", content = @Content),
		      @ApiResponse(responseCode = "409", description = "Un partant avec ce nom et numéro existe déja", content = @Content)})
	public ResponseEntity<T> createPartant(@Valid @RequestBody CreatePartantRequest partantRequest, Errors errors){
		
		log.info("Début du webservice createPartant");
		
		if (errors.hasErrors()) {
			String erreur = Utils.getValidationErrors(errors);
			return (ResponseEntity<T>) ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new Erreur(2, erreur));
	    }
		
		final CreateFullCourseResponse response = controller.createFullCourse(partantRequest);
		if (response.getErreur() != null) {
            return (ResponseEntity<T>) ResponseEntity.status(response.getErreur().getStatus())
                    .body(new Erreur(response.getErreur()));
        }
		
		log.info("Fin du webservice createPartant");
		return ResponseEntity.status(HttpStatus.CREATED).body(null);
	}
}
