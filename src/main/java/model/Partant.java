package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.criterion.Property;

@Entity
@Table(name = "partant")
public class Partant {
	
	@Id
    @GeneratedValue
    @Column(name = "id_partant")
    private long idPartant;

    @Column(name = "nom", nullable = false)
    private String nom;
    public static final Property PNOM = Property.forName("nom");
    
    @Column(name = "numero", nullable = false)
    private int numero;
    public static final Property PNUMERO = Property.forName("numero");
	public long getIdPartant() {
		return idPartant;
	}
	public void setIdPartant(long idPartant) {
		this.idPartant = idPartant;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
}
