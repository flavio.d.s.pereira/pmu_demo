package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.criterion.Property;

@Entity
@Table(name = "course_composition")
public class CourseComposition implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7969385249470394620L;
	
	@Id
	@GeneratedValue
	@Column(name = "id_course_composition")
	private long idCourseComposition;
	
	@Column(name = "numero_partant")
	private int numeroPartant; 
	
	@ManyToOne
	@JoinColumn(name = "id_course")
	private Course course;
    public static final Property PCOURSE = Property.forName("course");
	
	@ManyToOne
	@JoinColumn(name = "id_partant")
	private Partant partant;
    public static final Property PPARTANT = Property.forName("partant");

	public int getNumeroPartant() {
		return numeroPartant;
	}

	public void setNumeroPartant(int numeroPartant) {
		this.numeroPartant = numeroPartant;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Partant getPartant() {
		return partant;
	}

	public void setPartant(Partant partant) {
		this.partant = partant;
	}

	public long getIdCourseComposition() {
		return idCourseComposition;
	}

	public void setIdCourseComposition(long idCourseComposition) {
		this.idCourseComposition = idCourseComposition;
	}
}
