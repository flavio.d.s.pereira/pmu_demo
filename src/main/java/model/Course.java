package model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.criterion.Property;

@Entity
@Table(name = "course")
public class Course {

    @Id
    @GeneratedValue
    @Column(name = "id_course")
    private long idCourse;

    @Column(name = "nom", nullable = false)
    private String nom;
    public static final Property PNOM = Property.forName("nom");

    @Column(name = "numero", nullable = false)
    private long numero;
    public static final Property PNUMERO = Property.forName("numero");
    
    @Column(name = "date_course", nullable = false)
    private Date dateCourse;
    public static final Property PDATECOURSE = Property.forName("dateCourse");


	public long getIdCourse() {
		return idCourse;
	}


	public void setIdCourse(long idCourse) {
		this.idCourse = idCourse;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public long getNumero() {
		return numero;
	}


	public void setNumero(long numero) {
		this.numero = numero;
	}


	public Date getDateCourse() {
		return dateCourse;
	}


	public void setDateCourse(Date dateCourse) {
		this.dateCourse = dateCourse;
	}
}
