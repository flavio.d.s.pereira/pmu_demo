package kafka.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import api.utils.CreateFullCourseRequest;

@Service
public class KafkaProducer {

	
	private static final Logger logger = LoggerFactory.getLogger(KafkaProducer.class);
	
	private static final String TOPIC = "new_course";
	
	@Autowired
    private KafkaTemplate<String, CreateFullCourseRequest> kafkaTemplate;
	
	public void sendMessage(CreateFullCourseRequest message) {
        this.kafkaTemplate.send(TOPIC, message);
    }
}
