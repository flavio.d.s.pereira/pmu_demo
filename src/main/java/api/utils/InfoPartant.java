package api.utils;

import io.swagger.v3.oas.annotations.media.Schema;

public class InfoPartant {

	@Schema(description = "nom du partant.")
	private String nom;
	
	@Schema(description = "numéro du partant.")
	private int numero;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	} 
}
