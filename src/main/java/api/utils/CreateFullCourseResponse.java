package api.utils;

public class CreateFullCourseResponse {
	
	
	private ErreursRef erreur;

	public ErreursRef getErreur() {
		return erreur;
	}

	public void setErreur(ErreursRef erreur) {
		this.erreur = erreur;
	}

}
