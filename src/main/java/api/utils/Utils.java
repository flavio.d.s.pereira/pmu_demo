package api.utils;

import java.util.List;

import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

public class Utils {
	
	public static String getValidationErrors(Errors errors) {
		String erreur = "";
		List<ObjectError> errorsList = errors.getAllErrors();
		for(ObjectError err : errorsList) {
			erreur = erreur.concat(err.getDefaultMessage());
			erreur = erreur.concat(";");
		}
		
		return erreur;
	}

}
