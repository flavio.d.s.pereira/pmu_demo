package api.utils;

import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;

public class GetPartantListResponse extends CreateFullCourseResponse {
	
	@Schema(description = "Liste des partants.")
	private List<InfoPartant> partantList;

	public List<InfoPartant> getPartantList() {
		return partantList;
	}

	public void setPartantList(List<InfoPartant> partantList) {
		this.partantList = partantList;
	}
}
