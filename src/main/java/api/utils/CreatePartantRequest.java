package api.utils;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.v3.oas.annotations.media.Schema;

public class CreatePartantRequest {
	
	@Schema(description = "nom du partant.")
	@NotBlank
	private String nom;
	
	@Schema(description = "numéro du partant.")
	@NotNull
	private int numero;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

}
