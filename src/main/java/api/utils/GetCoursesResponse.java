package api.utils;

import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;

public class GetCoursesResponse extends CreateFullCourseResponse {
	
	@Schema(description = "Liste des courses existantes.")
	private List<CourseRepresentation> coursesList;
	
	public List<CourseRepresentation> getCoursesList() {
		return coursesList;
	}

	public void setCoursesList(List<CourseRepresentation> coursesList) {
		this.coursesList = coursesList;
	}
}
