package api.utils;

import io.swagger.v3.oas.annotations.media.Schema;

public class PartantCourseRepresentation {
	
	@Schema(description = "nom du partant.")
	private String nomPartant;
	
	@Schema(description = "numéro du partant.")
	private int numeroPartant;
	
	@Schema(description = "numéro du dossard du partant.")
	private int numeroDossard;

	public String getNomPartant() {
		return nomPartant;
	}

	public void setNomPartant(String nomPartant) {
		this.nomPartant = nomPartant;
	}

	public int getNumeroPartant() {
		return numeroPartant;
	}

	public void setNumeroPartant(int numeroPartant) {
		this.numeroPartant = numeroPartant;
	}

	public int getNumeroDossard() {
		return numeroDossard;
	}

	public void setNumeroDossard(int numeroDossard) {
		this.numeroDossard = numeroDossard;
	}

}
