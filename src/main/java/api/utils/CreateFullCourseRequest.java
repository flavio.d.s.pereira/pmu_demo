package api.utils;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Objet d'entrée pur la création d'une course.")
public class CreateFullCourseRequest {
	
	@Schema(description = "nom de la course.")
	@NotBlank(message = "le nom est obligatoire")
	private String nom;
	
	@Schema(description = "numéro de la course.")
	@NotNull(message = "le numero est obligatoire")
	private Long numero;
	
	@Schema(description = "Date de la course au format yyyy-MM-dd")
	@NotNull(message = "la date est obligatoire")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dateCourse;
	
	@Schema(description = "Liste des partants de la course")
	@NotNull
	private List<CreatePartantRequest> partantsList;

	public List<CreatePartantRequest> getPartantsList() {
		return partantsList;
	}

	public void setPartantsList(List<CreatePartantRequest> partantsList) {
		this.partantsList = partantsList;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public Date getDateCourse() {
		return dateCourse;
	}

	public void setDateCourse(Date dateCourse) {
		this.dateCourse = dateCourse;
	}
	
}
