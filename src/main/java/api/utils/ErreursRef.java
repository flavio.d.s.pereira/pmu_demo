package api.utils;

import org.springframework.http.HttpStatus;

public enum ErreursRef {
	
	CDE_ERROR_UNKNOWN(-1, "Erreur inconnue : ",HttpStatus.INTERNAL_SERVER_ERROR),
	COURSES_NOT_FOUND(1, "Aucune course trouvee", HttpStatus.NOT_FOUND),
	COURSE_ALREADY_EXISTS(2, "La course existe déja", HttpStatus.CONFLICT),
	PARTANTS_NOT_FOUND(3, "Partant non trouvé",  HttpStatus.NOT_FOUND),
	PARTANT_ALREADY_EXIST(4, "Un partant avec ce nom et numéro existe déja",  HttpStatus.CONFLICT),
	MINIMUM_PARTANT_NOT_SATISFIED(5, "La course doit posséder au minimum 3 partants", HttpStatus.BAD_REQUEST);
	
	private int code;
    private String message;
    private HttpStatus status;

    ErreursRef(final int code, final String message, final HttpStatus status) {
        this.code = code;
        this.message = message;
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getStatus() {
        return status;
    }
	
	

}
