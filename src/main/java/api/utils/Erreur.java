package api.utils;

public class Erreur {

    private int code;
    private String message;

    public Erreur(ErreursRef erreurEcl) {
        this.code = erreurEcl.getCode();
        this.message = erreurEcl.getMessage();
    }
    
    public Erreur(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
