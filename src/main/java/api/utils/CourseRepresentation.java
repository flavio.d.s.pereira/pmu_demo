package api.utils;

import java.util.Date;
import java.util.List;

public class CourseRepresentation {
	
	private String nomCourse;
	
	private String numeroCourse;
	
	private Date dateCourse;
		
	private List<PartantCourseRepresentation> participants;

	public String getNomCourse() {
		return nomCourse;
	}

	public void setNomCourse(String nomCourse) {
		this.nomCourse = nomCourse;
	}

	public String getNumeroCourse() {
		return numeroCourse;
	}

	public void setNumeroCourse(String numeroCourse) {
		this.numeroCourse = numeroCourse;
	}

	public Date getDateCourse() {
		return dateCourse;
	}

	public void setDateCourse(Date dateCourse) {
		this.dateCourse = dateCourse;
	}

	public List<PartantCourseRepresentation> getParticipants() {
		return participants;
	}

	public void setParticipants(List<PartantCourseRepresentation> participants) {
		this.participants = participants;
	}
}
