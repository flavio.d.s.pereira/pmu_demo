package controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import api.utils.Constantes;
import api.utils.CourseRepresentation;
import api.utils.CreateFullCourseRequest;
import api.utils.CreateFullCourseResponse;
import api.utils.CreatePartantRequest;
import api.utils.DeleteCourseResponse;
import api.utils.ErreursRef;
import api.utils.GetCoursesResponse;
import api.utils.PartantCourseRepresentation;
import dao.repo.CourseCompositionRepository;
import dao.repo.CourseRepository;
import dao.repo.PartantRepository;
import kafka.service.KafkaProducer;
import model.Course;
import model.CourseComposition;
import model.Partant;

@Component
public class CoursesController {
	
	private static final Logger log = Logger.getLogger(Constantes.WES_LOGGER);
	
	@Autowired
    CourseRepository courseRepository;
	
	@Autowired
	PartantRepository partantRepository;
	
	@Autowired
	CourseCompositionRepository courseCompositionRepository;
	
	@Autowired
    KafkaProducer kafkaProducer;
	
	public GetCoursesResponse getCourses() {
		
		GetCoursesResponse response = new GetCoursesResponse();
		List<CourseRepresentation> coursesRepList = new ArrayList<CourseRepresentation>();
		
		try {
			List<Course> coursesList = courseRepository.findAll();
			for(Course course : coursesList) {
				CourseRepresentation courseRepr = new CourseRepresentation();
				List<CourseComposition> courseCompoList = courseCompositionRepository.getCourseCompositionByCourse(course);
				List<PartantCourseRepresentation> partantCompoList = new ArrayList<PartantCourseRepresentation>();
				for(CourseComposition compo : courseCompoList) {
					Partant partant = compo.getPartant();
					int dossard =  compo.getNumeroPartant();
					PartantCourseRepresentation partantCourse = new PartantCourseRepresentation();
					partantCourse.setNomPartant(partant.getNom());
					partantCourse.setNumeroPartant(partant.getNumero());
					partantCourse.setNumeroDossard(dossard);
					partantCompoList.add(partantCourse);
				}
				courseRepr.setNomCourse(course.getNom());
				courseRepr.setNumeroCourse(String.valueOf(course.getNumero()));
				courseRepr.setDateCourse(course.getDateCourse());
				courseRepr.setParticipants(partantCompoList);
				
				coursesRepList.add(courseRepr);
			}
			
			response.setCoursesList(coursesRepList);
		
			
		}catch(Exception ex) {
			log.error(ex.getMessage(), ex);
			response.setErreur(ErreursRef.CDE_ERROR_UNKNOWN);
		}
		
		return response;
	}
	
	public CreateFullCourseResponse createFullCourse(CreateFullCourseRequest courseRequest){
		
		CreateFullCourseResponse response = new CreateFullCourseResponse();
		List<Partant> partantList = new ArrayList<Partant>();
		
		log.info("dateCourse = " + courseRequest.getDateCourse());
		log.info("nomCourse = " + courseRequest.getNom());
		log.info("numeroCourse = " + courseRequest.getNumero());
		
		try {
			
			Date date = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(courseRequest.getDateCourse());
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			date = cal.getTime();
			courseRequest.setDateCourse(date);
			
			Course course = courseRepository.findCourseByNameNumberAndDate(courseRequest.getNom(), courseRequest.getNumero(), courseRequest.getDateCourse());
			if(course != null) {
				log.error(ErreursRef.COURSE_ALREADY_EXISTS.getMessage());
				response.setErreur(ErreursRef.COURSE_ALREADY_EXISTS);
				return response;
			}
			
			List<CreatePartantRequest> partantListRequest = courseRequest.getPartantsList();
			for(CreatePartantRequest part : partantListRequest) {
				log.info("partant = " + part);
				Partant partant = partantRepository.findPartantByNom(part.getNom());
				if(partant == null) {
					log.error(ErreursRef.PARTANTS_NOT_FOUND.getMessage());
					response.setErreur(ErreursRef.PARTANTS_NOT_FOUND);
					return response;
				}
				else {
					partantList.add(partant);
				}
			}
			
			if(partantList.size() < 3) {
				//return error minimum 3 participants
				log.error(ErreursRef.MINIMUM_PARTANT_NOT_SATISFIED.getMessage());
				response.setErreur(ErreursRef.MINIMUM_PARTANT_NOT_SATISFIED);
				return response;
			}
			
			course = new Course();
			course.setNom(courseRequest.getNom());
			course.setNumero(courseRequest.getNumero());
			course.setDateCourse(courseRequest.getDateCourse());
			courseRepository.save(course);
			
			for(Partant partant : partantList) {
				CourseComposition compo = new CourseComposition();
				compo.setCourse(course);
				compo.setPartant(partant);
				compo.setNumeroPartant(partantList.indexOf(partant) + 1);
				courseCompositionRepository.save(compo);
			}
			
			//kafkaProducer.sendMessage("OK COURSE");
		}
		catch(Exception ex){
			log.error(ex.getMessage(), ex);
			response.setErreur(ErreursRef.CDE_ERROR_UNKNOWN);
		}
		
		return response;
		
	}
	
	public DeleteCourseResponse deleteCourse(long courseId) {
		
		DeleteCourseResponse response = new DeleteCourseResponse();
		
		try {
			Course courseToDelete = courseRepository.getOne(courseId);
			if(courseToDelete == null) {
				log.error(ErreursRef.COURSES_NOT_FOUND.getMessage());
				response.setErreur(ErreursRef.COURSES_NOT_FOUND);
				return response;
			}
			
			List<CourseComposition> compoList = courseCompositionRepository.getCourseCompositionByCourse(courseToDelete);
			courseCompositionRepository.deleteAll(compoList);
			courseRepository.delete(courseToDelete);	
		}
		catch(Exception ex){
			log.error(ex.getMessage(), ex);
			response.setErreur(ErreursRef.CDE_ERROR_UNKNOWN);
		}
		
		return response;
	}
	

}
