package controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import api.utils.Constantes;
import api.utils.CreateFullCourseResponse;
import api.utils.CreatePartantRequest;
import api.utils.ErreursRef;
import api.utils.GetPartantListResponse;
import api.utils.InfoPartant;
import dao.repo.PartantRepository;
import model.Partant;

@Component
public class PartantController {
	
	private static final Logger log = Logger.getLogger(Constantes.WES_LOGGER);
	
	@Autowired
    PartantRepository partantRepository;
	
	public GetPartantListResponse getPartantsList() {
		
		GetPartantListResponse resp = new GetPartantListResponse();
		
		try {
			List<Partant> partantList = partantRepository.findAll();
			List<InfoPartant> infoPartantList = new ArrayList<InfoPartant>();
			for(Partant part : partantList) {
				InfoPartant partant = new InfoPartant();
				partant.setNom(part.getNom());
				partant.setNumero(part.getNumero());
				infoPartantList.add(partant);
			}
			resp.setPartantList(infoPartantList);
		}catch(Exception ex) {
			log.error(ex.getMessage(), ex);
			resp.setErreur(ErreursRef.CDE_ERROR_UNKNOWN);
		}
		
		return resp;
		
	}
	
	public CreateFullCourseResponse createFullCourse(CreatePartantRequest partantRequest){
		
		CreateFullCourseResponse response = new CreateFullCourseResponse();
		
		try {
		
			Partant existsPartant = partantRepository.findPartantByNomAndNumero(partantRequest.getNom(), partantRequest.getNumero());
			
			if(existsPartant != null) {
				log.error(ErreursRef.PARTANT_ALREADY_EXIST.getMessage());
				response.setErreur(ErreursRef.PARTANT_ALREADY_EXIST);
				return response;
			}
			
			Partant partant = new Partant();
			partant.setNom(partantRequest.getNom());
			partant.setNumero(partantRequest.getNumero());
			partantRepository.save(partant);
			
		}catch(Exception ex) {
			log.error(ex.getMessage(), ex);
			response.setErreur(ErreursRef.CDE_ERROR_UNKNOWN);
		}
		
		return response;
	}

}
