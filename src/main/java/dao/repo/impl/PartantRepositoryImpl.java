package dao.repo.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import dao.repo.PartantRepositoryCustom;
import model.Partant;

public class PartantRepositoryImpl implements PartantRepositoryCustom {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public Partant findPartantByNomAndNumero(String nomPartant, int numero) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Partant> criteriaQuery = criteriaBuilder.createQuery(Partant.class);
    	Root<Partant> root = criteriaQuery.from(Partant.class);
    	criteriaQuery.select(root);
    	criteriaQuery.where(criteriaBuilder.equal(root.get(Partant.PNOM.getPropertyName()), nomPartant),
    			criteriaBuilder.equal(root.get(Partant.PNUMERO.getPropertyName()), numero));
    	
    	try {
    		return em.createQuery(criteriaQuery).setMaxResults(1).getSingleResult();
    	}catch(Exception ex){
    		return null;
    	}
		
	}

	@Override
	public Partant findPartantByNom(String nom) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Partant> criteriaQuery = criteriaBuilder.createQuery(Partant.class);
    	Root<Partant> root = criteriaQuery.from(Partant.class);
    	criteriaQuery.select(root);
    	criteriaQuery.where(criteriaBuilder.equal(root.get(Partant.PNOM.getPropertyName()), nom));
    	
    	try {
    		return em.createQuery(criteriaQuery).setMaxResults(1).getSingleResult();
    	}catch(Exception ex){
    		return null;
    	}
	}

}
