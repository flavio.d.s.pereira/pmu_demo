package dao.repo.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Service;

import dao.repo.CourseCompositionRepositoryCustom;
import model.Course;
import model.CourseComposition;

@Service
public class CourseCompositionRepositoryImpl implements CourseCompositionRepositoryCustom {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<CourseComposition> getCourseCompositionByCourse(Course course) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<CourseComposition> criteriaQuery = criteriaBuilder.createQuery(CourseComposition.class);
    	Root<CourseComposition> root = criteriaQuery.from(CourseComposition.class);
    	criteriaQuery.select(root);
    	criteriaQuery.where(criteriaBuilder.equal(root.get(CourseComposition.PCOURSE.getPropertyName()), course));
    	
    	try {
    		return em.createQuery(criteriaQuery).getResultList();
    	}catch(Exception ex){
    		return null;
    	}
	}

}
