package dao.repo.impl;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Service;

import dao.repo.CourseRepositoryCustom;
import model.Course;

@Service
public class CourseRepositoryImpl implements CourseRepositoryCustom {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public Course findCourseByNumber(String courseNumber) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Course> criteriaQuery = criteriaBuilder.createQuery(Course.class);
    	Root<Course> root = criteriaQuery.from(Course.class);
    	criteriaQuery.select(root);
    	criteriaQuery.where(criteriaBuilder.equal(root.get(Course.PNUMERO.getPropertyName()), courseNumber));
    	
    	try {
    		return em.createQuery(criteriaQuery).setMaxResults(1).getSingleResult();
    	}catch(Exception ex){
    		return null;
    	}
	}

	@Override
	public Course findCourseByNameNumberAndDate(String nom, Long numero, Date date) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Course> criteriaQuery = criteriaBuilder.createQuery(Course.class);
    	Root<Course> root = criteriaQuery.from(Course.class);
    	criteriaQuery.select(root);
    	criteriaQuery.where(criteriaBuilder.equal(root.get(Course.PNUMERO.getPropertyName()), numero),
    			criteriaBuilder.equal(root.get(Course.PNOM.getPropertyName()), nom),
    			criteriaBuilder.equal(root.get(Course.PDATECOURSE.getPropertyName()), date));
    	
    	try {
    		return em.createQuery(criteriaQuery).setMaxResults(1).getSingleResult();
    	}catch(Exception ex){
    		return null;
    	}
	}

}
