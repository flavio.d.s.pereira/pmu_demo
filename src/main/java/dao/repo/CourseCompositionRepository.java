package dao.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import model.CourseComposition;

@Repository
public interface CourseCompositionRepository extends JpaRepository<CourseComposition, Long>, CourseCompositionRepositoryCustom {

}
