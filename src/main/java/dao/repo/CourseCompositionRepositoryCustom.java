package dao.repo;

import java.util.List;

import model.Course;
import model.CourseComposition;

public interface CourseCompositionRepositoryCustom {
	
	List<CourseComposition> getCourseCompositionByCourse(Course course);
}
