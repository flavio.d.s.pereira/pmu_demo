package dao.repo;

import model.Partant;

public interface PartantRepositoryCustom {
	
	Partant findPartantByNomAndNumero(String nom, int numero);
	
	Partant findPartantByNom(String nom);

}
