package dao.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import model.Partant;

public interface PartantRepository extends JpaRepository<Partant, Long>, PartantRepositoryCustom {

}
