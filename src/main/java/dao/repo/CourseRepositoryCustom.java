package dao.repo;

import java.util.Date;

import model.Course;

public interface CourseRepositoryCustom {

    Course findCourseByNumber(String courseNumber); 
    
    Course findCourseByNameNumberAndDate(String nom, Long numero, Date date);
	
}
