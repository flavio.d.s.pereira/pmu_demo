Lien swagger pour tester: http://localhost:8080/api/v1/swagger-ui/index.html

2 API créés: 
 - ParticipantService: Cette api permet de réaliser la création de nouveaux participants. Pour qu'un participant puisse être lié dans une course,
   il faut qu il soit créé préalablement dans le référentiel des participants via l'opération de création présente dans cette API

 - CourseService: Cette api permet de réaliser la création de nouvelles courses


Model de données: 
- 3 tables 
   - Course: Table qui permet de stocker les données d'une course
   - Participant: Table qui permet de stocker les données d'un participant
   - Course_Composition: Table qui permet de stocker les participants d'une course. Ici se trouve le numéro de dossard attribué au participant